package com.example.my_spring_security.domain.service;

import com.example.my_spring_security.domain.User;

public interface UserService {
    User get(Long id);
    boolean save(User user);

    User getByPhone(String phone);
}
