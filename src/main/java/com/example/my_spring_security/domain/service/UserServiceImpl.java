package com.example.my_spring_security.domain.service;

import com.example.my_spring_security.domain.User;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Override
    public User get(Long id) {
        return null;
    }

    @Override
    public boolean save(User user) {
        return false;
    }

    @Override
    public User getByPhone(String phone) {
        return null;
    }
}
